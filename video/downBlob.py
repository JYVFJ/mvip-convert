
import requests
import re
import json
import pprint
import subprocess  # python 内置模块 不需要 install
import os

if __name__ == '__main__':
    url = "https://n6avpjtiow5vo7.xyz/9cf6c5d3-6094-4cbe-8b5d-8f5c45c16481"

    # 访问起始网页需添加的请求头，不加的话，得不到完整的源代码（反爬）
    base_headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 SE 2.X MetaSr 1.0',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'zh-CN,zh;q = 0.9'
    }

    # 请求视频下载地址时需要添加的请求头
    download_headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 SE 2.X MetaSr 1.0',
        'Referer': 'https://n6avpjtiow5vo7.xyz/explorePlay?id=704520',
        'Origin': 'https://n6avpjtiow5vo7.xyz',
        'Host': 'upos-hz-mirrorkodou.acgvideo.com',
        'Accept': '*/*',
        'Accept-Encoding': 'gzip, deflate, sdch, br',
        'Accept-Language': 'zh-CN,zh;q=0.8'
    }
    response = requests.get(url, headers=base_headers)
    html = response.text
    print(html)
    video_data = response.content

    with open("hs.mp4","wb") as file:
        file.write(video_data)
    print("视频保存成功")
    file.close()
