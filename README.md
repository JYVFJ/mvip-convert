# 项目由来

​		某天，坐在公司日常写bug，突然接到一个业务，批量重命名2000张图片的名称为 ****

​		作为一个程序员来说 这不是什么难事，可是普通运营人员根本不会操作，或者操作错误立马就呼叫你...

当你处理完这个奇葩需求后 我以为类似的需求不会再出现...

> 突然又接到一个需求，把`wmf`文件显示在浏览器中...
>
> `wmf`是一种特殊的矢量图格式，目前浏览器都是以主流的`png` `jpg` 为主，要想显示除非上传图片的时候转换为`png` 或者`jpg`  或者批量转换后再上传，前者在数据量小的时候可以操作，可是我这个将近有10000张...  

没有办法 在`java`中尝试转换一下吧，可是Java中找了好久没有找到时候的转换方法(可能是java不擅长吧) 灵机一动 我又把目标看向了`go`

​	可是go也跟Java一样不怎么争气  兜兜转转一大圈  要么翻墙下载大佬依赖  要么就是各种问题...

​	绝望之际  我想到了`python`

> python:想不到吧  我几行代码就搞定了  我有现成的库
>
> java: ....
>
> go: ....
>
> 我：你牛逼

**有人问为什么不用第三方软件批量转换？**

​	特么那个收费啊   而且我动不动就几千张  CPU都给我干烧了

**为了这个破功能开这个项目？**

​	当然不指这一个功能了  计划开发如下图中功能

![image-20231008114205470](./doc/image-20231008114205470.png)



**环境**

- python版本 3.9.10
- vue3 系列



## PIP安装

python3 自带了pip无需安装 如果提示找不到命令  则需要手动添加环境变量
```sybase
pip install -r requirements.txt
```


## 安装模块

```bash
pip install Pillow
```

安装完成后，我们可以使用**import**语句导入`Pillow`库

```bash
from PIL import Image
```

通过导入**Image**模块，我们可以使用`Pillow`库提供的图像处理功能。



## 安装flask

```bash
pip install flask
pip install flask_cors
```





## 打开和保存图像

- **打开图像**。使用`Pillow`库可以轻松打开各种图像格式的文件。我们可以使用**open()**函数打开图像文件，并将其赋值给一个变量。

代码示例

```python
from PIL import Image
# 打开图像文件
image = Image.open("image.jpg")
```

我们使用**open()**函数打开了名为`image.jpg`的图像文件，并将其赋值给**image**变量。这样就可以在后续的代码中使用**image**对象进行图像处理。

- **保存图像**。Pillow库提供了**save()**方法，可以将处理后的图像保存为不同格式的文件。我们可以指定保存的文件名和保存的格式。

```python
from PIL import Image
# 打开图像文件
image = Image.open("image.jpg")

# 保存图像
image.save("output.png", "PNG")
```

在上面的例子中，我们使用**save()**方法将**image**对象保存为名为`output.png`的`PNG`格式文件。通过指定不同的格式，我们可以保存图像为`JPEG、PNG、BMP`等格式

## 基本图像操作

- **调整图像大小**。Pillow库提供了**resize()**方法，可以调整图像的大小。我们可以指定新的宽度和高度，也可以根据比例进行调整。

  ```python
  from PIL import Image
  # 打开图像文件
  image = Image.open("image.jpg")
  # 调整图像大小
  new_size = (800, 600)
  resized_image = image.resize(new_size)
  # 保存调整后的图像
  resized_image.save("resized_image.jpg")
  ```

  在上面的例子中，我们使用**resize()**方法将图像调整为800x600像素的大小，并将调整后的图像保存为`resized_image.jpg`文件。

- **裁剪图像**。 Pillow库的**crop()**方法可以用于裁剪图像。我们可以指定裁剪区域的左上角和右下角坐标。

  ```python
  from PIL import Image
  # 打开图像文件
  image = Image.open("image.jpg")
  # 裁剪图像
  box = (100, 100, 500, 400)
  cropped_image = image.crop(box)
  # 保存裁剪后的图像
  cropped_image.save("cropped_image.jpg")
  ```

  在上面的例子中，我们使用**crop()**方法裁剪图像，指定了左上角坐标为(100, 100)，右下角坐标为(500, 400)。裁剪后的图像被保存为`cropped_image.jpg`文件。

- **旋转图像**。Pillow库提供了**rotate()**方法，可以对图像进行旋转操作。我们可以指定旋转角度进行图像旋转。

  ```python
  from PIL import Image
  # 打开图像文件
  image = Image.open("image.jpg")
  # 旋转图像
  rotated_image = image.rotate(45)
  # 保存旋转后的图像
  rotated_image.save("rotated_image.jpg")
  ```

  在上面的例子中，我们使用**rotate()**方法将图像顺时针旋转45度，并将旋转后的图像保存为`rotated_image.jpg`文件



## 图像处理高级功能

- **图像缩略图**。Pillow库的**thumbnail()**方法可以生成图像的缩略图。我们可以指定缩略图的最大尺寸。

  ```python
  from PIL import Image
  # 打开图像文件
  image = Image.open("image.jpg")
  # 生成缩略图
  thumbnail_size = (200, 200)
  image.thumbnail(thumbnail_size)
  # 保存缩略图
  image.save("thumbnail.jpg")
  ```

  在上面的例子中，我们使用**thumbnail()**方法生成200x200像素的缩略图，并将缩略图保存为`humbnail.jpg`文件。

  

- **添加水印**。Pillow库提供了丰富的绘图功能，可以在图像上添加文本、形状等元素，实现水印效果。

  ```python
  from PIL import Image, ImageDraw, ImageFont
  # 打开图像文件
  image = Image.open("image.jpg")
  # 创建绘图对象
  draw = ImageDraw.Draw(image)
  # 添加水印文本
  text = "Watermark"
  font = ImageFont.truetype("arial.ttf", 36)
  text_size = draw.textsize(text, font)
  text_position = (image.width - text_size[0], image.height - text_size[1])
  draw.text(text_position, text, fill=(255, 255, 255), font=font)
  # 保存带水印的图像
  image.save("watermarked_image.jpg")
  ```

  在上面的例子中，我们使用**ImageDraw**模块创建了一个绘图对象，并使用**text()**方法在图像上添加了水印文本。通过指定文本的位置、颜色和字体等参数，我们可以自定义水印效果。





## 项目部署

前端使用vue，将vue打包后放到templates目录下 



## 打包成EXE

安装依赖

```bash
pip install pyinstaller
```

**常用参数**

```
-h 查看帮助
-w 忽略控制台，打包gui软件时使用
-F dist目录中只生成一个exe文件
-p 表示你自己定义需要加载的类库的路径
-D 创建dist目录，里面包含exe以及其他一些依赖性文件（默认，可不添加）
-i 指定打包程序使用的图标文件
```

**命令使用**

使用cmd进入到项目文件夹，执行如下命令：

```bash
pyinstaller -i ico.png -F -w demo.py --add-data "templates;templates"
```



### 两种打包方式

#### 文件夹模式onedir

**默认方式，使用命令：**`pyinstaller fileren.py`

执行完命令后，在项目文件夹下多出了三个文件，`build`，`dist`和`fileren.spec`、`__pycache__`。

- `build`文件夹用于存储日志文件。
- `dist`文件夹储存可执行文件即相关依赖。
- `__pycache__`文件夹里是`Python`版本信息。
- `fileren.spec`打包的配置文件，可以配置依赖资源。

除了`dist`文件夹，其它都可以删除，没什么用

这种模式下，需要把整个dist文件夹发给别人才能运行。

#### 单文件模式onefile

加上-F参数，全部的依赖文件都会被打包到exe文件中，在dist文件夹中只有一个可执行文件，

把这个可执行文件发给别人就可以直接运行了。

```bash
pyinstaller -w -F fileren.py
```

默认的应用图标，有点丑，介绍一个非常实用的图标网站，找一个好看点的图标。[https://www.easyicon.net/](https://link.zhihu.com/?target=https%3A//www.easyicon.net/)

然后加上-i参数，重新打包

```bash
pyinstaller -F -w -i folder.ico fileren.py
```

#### 注意事项

**1.** 有时候，除了代码本身，还包括一些外部资源文件，如图片、配置文件等。可以修改第一次打包完成的配置文件XXX.spec配置文件，然后执行命令`pyinstaller xxx.spec`，便可按照spec文件中的新配置重新打包。

binaries元组，二进制文件（如.exe/.dll/.so等），比如binaries=[('ci64.dll','.'),('ABDLL64.dll','.')]

datas元组，非二进制文件（如图片文件、文本文件等），例如：datas=[('icons','icons’)]

**2.** 打包的文件很大，如何解决，最好的办法创建虚拟环境，比如：

```bash
pip install virtualenv
cd F:\env
virtualenv env01 #创建env01的虚拟环境
cd F:\env\env\Scripts
activate #激活虚拟环境
```









Vue 

首先在main.js里写一个我们要定义的全局变量，比如一个系统id吧

```vue
app.config.globalProperties.$systemId = "10"
import { getCurrentInstance } from "vue";
const systemId = getCurrentInstance()?.appContext.config.globalProperties.$systemId
console.log(systemId);//控制台可以看到输出了10
```

