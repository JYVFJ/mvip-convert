import webbrowser

from flask import Flask, request, jsonify, render_template
from application.images.ConvertImg.ImgTool import getImgBlueprint

"""
如果安装了pip install flask-cors 仍然提示找不到包  则使用下面方式指定安装的包位置

打包：
pyinstaller -F -i favicon32.ico main.py --add-data "templates;templates"  --add-data "static;static"

"""
import sys

sys.path.append("d:\python\python39\lib\site-packages")

from flask_cors import CORS

# 映射静态资源
app = Flask(__name__, static_folder='static', static_url_path='/', template_folder='templates')
app.register_blueprint(getImgBlueprint())


@app.route('/', methods=['GET'])
def Index():
    return render_template('index.html')


def open_browser():
    webbrowser.open("http://127.0.0.1:7777")


CORS(app, resources=r'/*')
if __name__ == '__main__':
    # threading.Timer(1, open_browser).start()
    app.debug = True
    app.run(host="0.0.0.0", port=7777)
