import request from '@/utils/http'

export const imgToIcon = (data) => {
    return request({
      url: '/imgToIcon',
      method: 'POST',
      data: data
    })
}


export const transfer = (data) => {
  return request({
    url: '/transfer',
    method: 'POST',
    data: data
  })
}
