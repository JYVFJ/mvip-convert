import {StrUtils} from './utils.js'

/**
 * Unicode 转中文
 * @param {*} unicodeStr Unicode 字符串 
 * @returns 中文
 */
export const uniCode2zhCn = (unicodeStr) => {
    
    if(StrUtils.isEmpty(unicodeStr)){
        return 
    }
    
    let str = '';
    const codeList = unicodeStr.match(/\\u[\dA-Fa-f]{4}/gm);
    for (let code of codeList) {
        const unicode = parseInt(code.slice(2), 16);
        str += String.fromCharCode(unicode);
    }
    return str
}

/**
 * 字符串转Unicode编码
 * @param {*} str  待转换的字符串
 * @returns  转换成功后的字符串Unicode编码
 */
export const zhCn2UniCode = (str) => {
   let unicodeStr = "" 
   for(let i = 0 ;i <str.length;i++){
    const unicode = str.charCodeAt(i);
    unicodeStr += `\\u${unicode.toString(16).toUpperCase()}`;
   }
   return unicodeStr
}