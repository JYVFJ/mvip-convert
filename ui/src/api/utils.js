const StrUtils = {

    /**
     * 字符串是否为空
     * @param {*} str 
     * @returns true 为true
     */
    isEmpty(str){
        if(!str){
            return true
        }
        if(typeof str == 'string'){
            if(str.trim().length == 0){
                return true
            }
        }

        if(typeof str == 'number'){
            if(str <= 0){
                return true
            }
        }
        return false
    }
}

export {
    StrUtils
} 