// createRouter：创建router实例对象
// createWebHistory：创建history模式的路由

import { createRouter, createWebHashHistory } from 'vue-router'
import Layout from '@/layout/index.vue'
import Home from '@/views/Home/index.vue'
import transfer from '@/views/Home/transfer.vue'
import recommend from '@/views/preview/index.vue'
import icon from '@/views/Home/icon.vue'
import qyk from '@/views/gpt/qyk.vue'

const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),
  // path和component对应关系的位置
  routes: [
    {
      path: '/',
      component: Layout,
      children: [
        {
          path: '',
          name:"首页",
          component: Home,
          meta: {
            title: "首页",
            name:"home",
            icon: "home",
            affix: true,
            keepAlive: true,
            alwaysShow: false,
          }
        },
        {
          path: '/icon',
          name: 'icon',
          component: icon,
          meta: {
            title: "转ICO",
            icon: "icon",
            name: 'icon',
            affix: true,
            keepAlive: true,
            alwaysShow: false,
          }
        },
        {
          path: '/shortURL',
          name: 'shortURL',
          component: transfer,
          meta: {
            title: "短网址",
            icon: "shortURL",
            name: 'shortURL',
            affix: true,
            keepAlive: true,
            alwaysShow: false,
          }
        },
        {
          path:"/recommend",
          name:"recommend",
          component: recommend,
          meta: {
            title: "推荐",
            name: 'recommend',
            icon: "recommend",
            affix: true,
            keepAlive: true,
            alwaysShow: false,
          }
        },
        {
          path:"/qyk",
          name:"qyk",
          component: qyk,
          meta: {
            title: "青云客",
            name: 'qyk',
            icon: "qyk",
            affix: true,
            keepAlive: true,
            alwaysShow: false,
          }
        },
        {
          path: '/404',
          name:'404',
          component:()=>import('@/views/404/index.vue')
        }
      ]
    },
    {
      path: '/',
      name: '主页',
      redirect: '/index'
    },
    {
      path: '/personal',
      component: Layout,
      children: [{
        path: '',
        name: '个人中心',
        component: () => import( '@/views/personal/index.vue'),
      }]
    },
    { path: '/:path(.*)*', redirect: '/404', hidden: true }
  ],
  // 路由滚动行为定制
  scrollBehavior () {
    return {
      top: 0
    }
  }
})

export default router
