
// 获取assets静态资源
const getAssetsFile = (url) => {
    if(!url){
        return "/src/assets/icons/hot.png"
    }
    return `/src/assets/icons/${url}`
}

export  {
    getAssetsFile
}