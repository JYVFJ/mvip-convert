import {h } from 'vue'
import { NIcon, useMessage } from 'naive-ui'
import {
    BookOutline as BookIcon,
    PersonOutline as PersonIcon,
    WineOutline as WineIcon
  } from '@vicons/ionicons5'

function renderIcon (icon) {
    return () => h(NIcon, null, { default: () => h(icon) })
  }

export const menuData = [
    {
        whateverLabel: '且听风吟',
        whateverKey: 'hear-the-wind-sing',
        icon: renderIcon(BookIcon)
    },
    {
        whateverLabel: '1973年的弹珠玩具',
        whateverKey: 'pinball-1973',
        icon: renderIcon(BookIcon),
        disabled: true,
        whateverChildren: [
            {
                whateverLabel: '鼠',
                whateverKey: 'rat'
            }
        ]
    },
    {
        whateverLabel: '寻羊冒险记',
        whateverKey: 'a-wild-sheep-chase',
        disabled: true,
        icon: renderIcon(BookIcon)
    },
    {
        whateverLabel: '舞，舞，舞',
        whateverKey: 'dance-dance-dance',
        icon: renderIcon(BookIcon),
        whateverChildren: [
            {
                type: 'group',
                whateverLabel: '人物',
                whateverKey: 'people',
                whateverChildren: [
                    {
                        whateverLabel: '叙事者',
                        whateverKey: 'narrator',
                        icon: renderIcon(PersonIcon)
                    },
                    {
                        whateverLabel: '羊男',
                        whateverKey: 'sheep-man',
                        icon: renderIcon(PersonIcon)
                    }
                ]
            },
            {
                whateverLabel: '饮品',
                whateverKey: 'beverage',
                icon: renderIcon(WineIcon),
                whateverChildren: [
                    {
                        whateverLabel: '威士忌',
                        whateverKey: 'whisky'
                    }
                ]
            },
            {
                whateverLabel: '食物',
                whateverKey: 'food',
                whateverChildren: [
                    {
                        whateverLabel: '三明治',
                        whateverKey: 'sandwich'
                    }
                ]
            },
            {
                whateverLabel: '过去增多，未来减少',
                whateverKey: 'the-past-increases-the-future-recedes'
            }
        ]
    }
]