import { createApp } from 'vue'
import App from './App.vue'
import { getAssetsFile } from '@/utils/assetsFile.js'
import pinia from './stores'
import router from './router'
import { setHtmlTheme } from '@/utils/style.js'
import Elementplus from "element-plus"
import 'element-plus/dist/index.css'; // 引入ElementPlus的全局样式
import 'element-plus/theme-chalk/dark/css-vars.css' // 暗黑主题

// 引入全局样式
// import './styles/common.scss'

// 引入阿里图标
import './assets/icon/iconfont.css'

const app = createApp(App)
app.config.globalProperties.$getAssetsFile = getAssetsFile
app.use(pinia)
app.use(router)
app.use(Elementplus)

// Store 准备就绪后处理主题色
setHtmlTheme()

app.mount('#app')
