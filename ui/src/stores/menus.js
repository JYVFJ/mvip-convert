import {defineStore} from 'pinia'

const useMenus = defineStore("menus",{
    state:() => ({
        child:[],
        name:""
    }),
    
    getters: {
        
  	},

  	actions: {
        setMenuItem:function(name,item){
            this.name = name
            this.child = item
        }
  	}
})

//暴露这个useCounter模块
export default useMenus