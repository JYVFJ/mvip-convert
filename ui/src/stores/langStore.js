import {defineStore} from 'pinia'
import useSettingStore from '@/stores/settingStore.js'
import { StorageEnum } from '@/enums/storageEnum.js'
import { setLocalStorage } from '@/utils/storage.js'

const { GO_LANG_STORE } = StorageEnum
const langStore = defineStore("langStore",{
    state: () =>{
      storageLang : 'zh'
    },
    getters: {
      getLang() {
        return this.lang
      }
    },
    actions: {
      changeLang(lang){
        const settingStore = useSettingStore()
        if (this.lang === lang) return
        this.lang = lang
        setLocalStorage(GO_LANG_STORE, this.$state)
  
        if (settingStore.getChangeLangReload) {
        //   reloadRoutePage()
        }
      }
    }
})

//暴露这个useCounter模块
export default langStore