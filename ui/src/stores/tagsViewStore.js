import { defineStore } from 'pinia'

// 多标签页
const tagsViewStore = defineStore("tagsViewStore", {
    state: () => ({
        visitedViews: [],
        cachedViews: []
    }),

    getters: {

    },

    actions: {
        /**
         添加已访问视图到已访问视图列表中
         TagView {
            name: string; // 页签名称
            title: string; // 页签标题
            path: string; //页签路由路径
            fullPath: string; // 页签路由完整路径
            icon?: string; // 页签图标
            affix?: boolean; // 是否固定页签
            keepAlive?: boolean; // 是否开启缓存
            query?: any; // 路由查询参数
            }
         * @param {*} view 
         * @returns 
         */
        addVisitedView(view) {
            // 如果已经存在于已访问的视图列表中，则不再添加
            if (this.visitedViews.some((v) => v.path === view.path)) {
                return;
            }
            // 如果视图是固定的（affix），则在已访问的视图列表的开头添加
            if (view.affix) {
                this.visitedViews.unshift(view);
            } else {
                // 如果视图不是固定的，则在已访问的视图列表的末尾添加
                this.visitedViews.push(view);
            }
        },

        addCachedView(view) {
            const viewName = view.name
            
            // 如果缓存视图名称已经存在于缓存视图列表中，则不再添加
            if (this.cachedViews.includes(viewName)) {
                return;
            }

            // 如果视图需要缓存（keepAlive），则将其路由名称添加到缓存视图列表中
            if (view.keepAlive) {
                this.cachedViews.push(viewName);
            }
            console.log(this.cachedViews)
        },
        delVisitedView(view) {
            return new Promise((resolve) => {
                for (const [i, v] of this.visitedViews.entries()) {
                    // 找到与指定视图路径匹配的视图，在已访问视图列表中删除该视图
                    if (v.path === view.path) {
                        this.visitedViews.splice(i, 1);
                        break;
                    }
                }
                resolve([...this.visitedViews]);
            });
        },
        delCachedView(view) {
            const viewName = view.name;
            return new Promise((resolve) => {
                const index = this.cachedViews.indexOf(viewName);
                index > -1 && this.cachedViews.splice(index, 1);
                resolve([...this.cachedViews]);
            });
        },
        delOtherVisitedViews(view) {
            return new Promise((resolve) => {
                this.visitedViews = this.visitedViews.filter((v) => {
                    return v?.affix || v.path === view.path;
                });
                resolve([...this.visitedViews]);
            });
        },
        delOtherCachedViews(view) {
            const viewName = view.name ;
            return new Promise((resolve) => {
                const index = this.cachedViews.indexOf(viewName);
                if (index > -1) {
                    this.cachedViews = this.cachedViews.slice(index, index + 1);
                } else {
                    // if index = -1, there is no cached tags
                    this.cachedViews = [];
                }
                resolve([...this.cachedViews]);
            });
        },
        updateVisitedView(view) {
            for (let v of this.visitedViews) {
                if (v.path === view.path) {
                    v = Object.assign(v, view);
                    break;
                }
            }
        },
        addView(view) {
            this.addVisitedView(view);
            this.addCachedView(view);
        },
        delView(view) {
            return new Promise((resolve) => {
                this.delVisitedView(view);
                this.delCachedView(view);
                resolve({
                    visitedViews: [...this.visitedViews],
                    cachedViews: [...this.cachedViews],
                });
            });
        },
        delOtherViews(view) {
            return new Promise((resolve) => {
                delOtherVisitedViews(view);
                delOtherCachedViews(view);
                resolve({
                    visitedViews: [...this.visitedViews],
                    cachedViews: [...this.cachedViews],
                });
            });
        },
        delLeftViews(view) {
            return new Promise((resolve) => {
                const currIndex = this.visitedViews.findIndex(
                    (v) => v.path === view.path
                );
                if (currIndex === -1) {
                    return;
                }
                visitedViews.value = this.visitedViews.filter((item, index) => {
                    if (index >= currIndex || item?.affix) {
                        return true;
                    }

                    const cacheIndex = this.cachedViews.indexOf(item.name);
                    if (cacheIndex > -1) {
                        this.cachedViews.splice(cacheIndex, 1);
                    }
                    return false;
                });
                resolve({
                    visitedViews: [...this.visitedViews],
                });
            });
        },
        delRightViews(view) {
            return new Promise((resolve) => {
                const currIndex = this.visitedViews.findIndex(
                    (v) => v.path === view.path
                );
                if (currIndex === -1) {
                    return;
                }
                this.visitedViews = this.visitedViews.filter((item, index) => {
                    if (index <= currIndex || item?.affix) {
                        return true;
                    }
                });
                resolve({
                    visitedViews: [...this.visitedViews],
                });
            });
        },
        delAllViews() {
            return new Promise((resolve) => {
                const affixTags = this.visitedViews.filter((tag) => tag?.affix);
                this.visitedViews = affixTags;
                this.cachedViews = [];
                resolve({
                    visitedViews: [...this.visitedViews],
                    cachedViews: [...this.cachedViews],
                });
            });
        },
        delAllVisitedViews() {
            return new Promise((resolve) => {
                const affixTags = this.visitedViews.filter((tag) => tag?.affix);
                this.visitedViews = affixTags;
                resolve([...this.visitedViews]);
            });
        },
        delAllCachedViews() {
            return new Promise((resolve) => {
                this.cachedViews = [];
                resolve([...this.cachedViews]);
            });
        }
    }
})

//暴露这个 tagsViewStore 模块
export default tagsViewStore