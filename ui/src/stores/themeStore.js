import { defineStore } from 'pinia'
import { ThemeEnum, themeColorName } from '@/enums/StyleEnum.js'
import { setLocalStorage } from '@/utils/storage.js'
import colorList from '@/data/Color.json'

// 主题
const themeStore = defineStore("themeStore", {
    state: () => ({
        darkTheme: true,  // 是否暗黑
        themeName: ThemeEnum.DARK, // dark | light 主题名称
        appTheme: "",   // 颜色色号
        appThemeDetail: ""

    }),

    getters: {
        getDarkTheme() {
            return this.darkTheme
        },
        getAppTheme() {
            return this.appTheme
        },
        getAppThemeDetail() {
            return this.appThemeDetail
        }
    },

    actions: {
        // 切换主题
        changeTheme() {
            this.darkTheme = !this.darkTheme
            this.themeName = this.darkTheme ? ThemeEnum.DARK : ThemeEnum.LIGHT
            setLocalStorage(themeColorName, this.$state)
        },
        // 设置颜色
        setAppColor(color) {
            this.appTheme = color.hex
            this.appThemeDetail = color
            console.log(color, "当前颜色")
            setLocalStorage(themeColorName, this.$state)
            let e = document.documentElement.style;
            e.setProperty('--color-primary', color.hex);
            e.setProperty('--color-primary-bg', `rgba(${color.RGB},0.18)`);


            e.setProperty('--el-button-bg-color', `rgba(${color.RGB},0.18)`);
            e.setProperty('--el-color-warning-light-9', `rgba(${color.RGB},0.18)`);
            e.setProperty('--el-color-warning-light-5', `rgba(${color.RGB},0.28)`);
            e.setProperty('--el-color-warning', `rgba(${color.RGB},1)`);
        },
        randomColor() {
            let index = Math.floor(Math.random() * (colorList.length - 1 + 1)) + 1;
            this.setAppColor(colorList[index])
        }
    }
})

//暴露这个 themeStore 模块
export default themeStore