import os
import argparse


# https://zhuanlan.zhihu.com/p/367299550?utm_id=0
def batch_rename(dir, old_ext, new_ext):
    """
    This will batch rename a group of files in a given directory,
    once you pass the current and new extensions
    :param dir:
    :param old_ext:
    :param new_ext:
    :return:
    """
    files = os.listdir(dir)
    # print(files)
    for filename in os.listdir(dir):
        # 返回的是是 tulple类型的　([文件名], [文件扩展名])
        splite_file = os.path.splitext(filename)
        print(splite_file)
        root_name, file_ext = splite_file
        if old_ext == file_ext:
            newfile = root_name + new_ext

            os.rename(
                os.path.join(dir, filename),
                os.path.join(dir, newfile)
            )

            print("rename is done!")
            print(os.listdir(dir))


def get_parser():
    parser = argparse.ArgumentParser(description='change extension of files in a working directory')
    parser.add_argument('dir', metavar='DIR', type=str, nargs=1, help='the directory where to change extension')
    parser.add_argument('old_ext', metavar='OLD_EXT', type=str, nargs=1, help='old extension')
    parser.add_argument('new_ext', metavar='NEW_EXT', type=str, nargs=1, help='new extension')
    return parser
