# 定义输入、输出路径
import os
from PIL import Image



# """
# 图片转换
# input_dir 输入路径
# output_dir 输出路径
# output_format 输出的格式名称
# """

### 图片转换
def PicTransfer(input_dir, output_dir, output_format):
    # 获取输入路径下的所有文件
    input_file_list = os.listdir(input_dir)
    pic_format = ['bmp', 'jpg', 'png', 'tif', 'gif', 'pcx', 'tga', 'exif', 'fpx', 'svg', 'psd', 'cdr', 'pcd',
                  'dxf', 'ufo', 'eps', 'ai', 'raw', 'wmf', 'webp', 'avif', 'apng']
    sum = 0
    for file in input_file_list:
        if '.' in file:
            pic_name = file.split('.')[0]  # 图片名称
            pic_ext = file.split('.')[1]  # 图片后缀
            if pic_ext in pic_format:  # 判断文件是否为图片
                if pic_ext != output_format:  # 只对不是输出图片格式的进行处理
                    img = Image.open(os.path.join(input_dir, file))  # 打开图片
                    try:
                        r, g, b, a = img.split()
                    except:
                        r, g, b = img.split()
                    img = Image.merge('RGB', (r, g, b))
                    img.save(os.path.join(output_dir, pic_name + '.' + output_format))  # 保存为你想要的图片格式
                    sum += 1
                    print(os.path.join(output_dir, pic_name + '.' + output_format), sum)
    return sum
# 使用方法，举例如下 os.getcwd()
# PicTransfer('D:/work/assets/wmf/16140000.wmf', 'D:/work/assets/wmf/16140000.png', 'png')
